#!/bin/bash
#Download Node Exporter
wget https://github.com/prometheus/node_exporter/releases/download/v1.3.1/node_exporter-1.3.1.linux-amd64.tar.gz
tar xvf node_exporter-1.3.1.linux-amd64.tar.gz
cd node_exporter-1.3.1.linux-amd64
sudo cp node_exporter /usr/local/bin
# Exit current directory
cd ..
# Remove the extracted directory
sudo rm -rf ./node_exporter-1.3.1.linux-amd64
#Create Node Exporter User
sudo useradd --no-create-home --shell /bin/false node_exporter
sudo chown node_exporter:node_exporter /usr/local/bin/node_exporter
#Create and start the Node Exporter service
sudo chmod 777 /etc/systemd/system/node_exporter.service
sudo rm -rf /etc/systemd/system/node_exporter.service
sudo mv node_exporter.service /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl start node_exporter
echo "test (http://your_server_ip:9100/metrics)"
#If port 9100 is unreachable
##As we mentioned previously, by default the node exporter service will run on the port 9100 of your server. If after starting the service, the service is unreachable, do not forget ##to open the port 9100 in your Ubuntu server. If you are using UFW (Uncomplicated Firewall), you can easily open this port using the following instruction:

###sudo ufw allow 9100

####Alternatively if you are using IPTABLES, use the following command to allow incoming traffic on that port instead:

####sudo iptables -I INPUT -p tcp -m tcp --dport 9100 -j ACCEPT

